/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2014 Universidad de la República - Uruguay
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Author: Matías Richart <mrichart@fing.edu.uy>
*/

/**
* This example program is designed to illustrate the behavior of
* rate-adaptive WiFi rate controls such as Minstrel.  Power-adaptive
* rate controls can be illustrated also, but separate examples exist for
* highlighting the power adaptation.
*
* This simulation consist of 2 nodes, one AP and one STA.
* The AP generates UDP traffic with a CBR of 54 Mbps to the STA.
* The AP can use any power and rate control mechanism and the STA uses
* only Minstrel rate control.
* The STA can be configured to move away from (or towards to) the AP.
* By default, the AP is at coordinate (0,0,0) and the STA starts at
* coordinate (5,0,0) (meters) and moves away on the x axis by 1 meter every
* second.
*
* The output consists of:
* - A plot of average throughput vs. distance.
* - (if logging is enabled) the changes of rate to standard output.
*
* Example usage:
* ./waf --run "rate-adaptation-distance --standard=802.11a --staManager=ns3::MinstrelWifiManager --apManager=ns3::MinstrelWifiManager --outputFileName=minstrel"
*
* Another example (moving towards the AP):
* ./waf --run "rate-adaptation-distance --standard=802.11a --staManager=ns3::MinstrelWifiManager --apManager=ns3::MinstrelWifiManager --outputFileName=minstrel --stepsSize=1 --STA1_x=-200"
*
* Example for HT rates with SGI and channel width of 40MHz:
* ./waf --run "rate-adaptation-distance --staManager=ns3::MinstrelHtWifiManager --apManager=ns3::MinstrelHtWifiManager --outputFileName=minstrelHt --shortGuardInterval=true --channelWidth=40"
*
* To enable the log of rate changes:
* export NS_LOG=RateAdaptationDistance=level_info
*/

#include "ns3/gnuplot.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/uinteger.h"
#include "ns3/boolean.h"
#include "ns3/log.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/mobility-helper.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/on-off-helper.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/mobility-model.h"
#include "ns3/waypoint.h"
#include "ns3/waypoint-mobility-model.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/ipv4-global-routing-helper.h"


#define PI 3.14159265

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("RateAdaptationDistance");


void DevMonitorSnifferRx (Ptr< const Packet > packet, uint16_t channelFreqMhz, WifiTxVector txVector, struct MpduInfo aMpdu, struct SignalNoiseDbm signalNoise) {
  std::cout << "rx," << Simulator::Now ().GetSeconds () << "," << signalNoise.signal << ", " << txVector.GetMode() << ", " << txVector.GetGuardInterval() << ", " << txVector.GetChannelWidth() << ", " << txVector.GetNss() << "\n";
}


void DevMonitorSnifferTx (Ptr< const Packet > packet, uint16_t channelFreqMhz, WifiTxVector txVector, struct MpduInfo aMpdu) {
  std::cout << "tx," << Simulator::Now ().GetSeconds () << "," << txVector.GetMode() << ", " << txVector.GetGuardInterval() << ", " << txVector.GetChannelWidth() << ", " << txVector.GetNss() << "\n";
}

void MobilityPrint (Ptr<Node> node) {
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  Vector pos = mobility->GetPosition ();
  Vector vel = mobility->GetVelocity ();

  Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
  Ipv4Address addr = ipv4->GetAddress (1, 0).GetLocal ();


  std::cout << "position," << Simulator::Now ().GetSeconds () << "," << addr << "," << pos << "," << vel << "\n";
  Simulator::Schedule (Seconds (0.1), &MobilityPrint, node);
}


int main (int argc, char *argv[])
{
  uint32_t rtsThreshold = 65535;
  std::string staManager = "ns3::IntelWifiManager";
  std::string apManager = "ns3::IntelWifiManager";
  std::string outputFileName = "Intel";
  std::string dataRate = "200Mb/s";
  uint32_t BeMaxAmpduSize = 65535;
  bool shortGuardInterval = true;
  uint32_t chWidth = 20;
  double distance = 30;
  double velocity = 1;
  double simulationTime = 0;
  bool verbose = false;

  CommandLine cmd;
  cmd.AddValue ("staManager", "PRC Manager of the STA", staManager);
  cmd.AddValue ("apManager", "PRC Manager of the AP", apManager);
  cmd.AddValue ("shortGuardInterval", "Enable Short Guard Interval in all stations", shortGuardInterval);
  cmd.AddValue ("channelWidth", "Channel width of all the stations", chWidth);
  cmd.AddValue ("rtsThreshold", "RTS threshold", rtsThreshold);
  cmd.AddValue ("BeMaxAmpduSize", "BE Mac A-MPDU size", BeMaxAmpduSize);
  cmd.AddValue ("outputFileName", "Output filename", outputFileName);
  cmd.AddValue ("distance", "Distance between the STA and the AP", distance);
  cmd.AddValue ("velocity", "Velocity of the STA", velocity);
  cmd.AddValue ("dataRate", "Velocity of the STA", dataRate);
  cmd.AddValue("verbose", "Verbose output (tx, rx)", verbose);
  cmd.Parse (argc, argv);


  // Define the APs
  NodeContainer wifiApNodes;
  wifiApNodes.Create (1);

  //Define the STAs
  NodeContainer wifiStaNodes;
  wifiStaNodes.Create (1);

  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  int nStreams = 2;
  wifiPhy.Set ("Antennas", UintegerValue (nStreams));
  wifiPhy.Set ("MaxSupportedTxSpatialStreams", UintegerValue (nStreams));
  wifiPhy.Set ("MaxSupportedRxSpatialStreams", UintegerValue (nStreams));

  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
  wifiPhy.SetChannel (wifiChannel.Create ());

  NetDeviceContainer wifiApDevices;
  NetDeviceContainer wifiStaDevices;
  NetDeviceContainer wifiDevices;

  WifiHelper wifi;
  wifi.SetStandard (WIFI_PHY_STANDARD_80211ac);
  WifiMacHelper wifiMac;

  //Configure the STA node
  wifi.SetRemoteStationManager (staManager, "RtsCtsThreshold", UintegerValue (rtsThreshold));

  Ssid ssid = Ssid ("AP");
  wifiMac.SetType ("ns3::StaWifiMac",
  "Ssid", SsidValue (ssid));
  wifiStaDevices.Add (wifi.Install (wifiPhy, wifiMac, wifiStaNodes.Get (0)));

  //Configure the AP node
  wifi.SetRemoteStationManager (apManager, "RtsCtsThreshold", UintegerValue (rtsThreshold));

  ssid = Ssid ("AP");
  wifiMac.SetType ("ns3::ApWifiMac",
  "Ssid", SsidValue (ssid));
  wifiApDevices.Add (wifi.Install (wifiPhy, wifiMac, wifiApNodes.Get (0)));

  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/BE_MaxAmpduSize", UintegerValue (BeMaxAmpduSize));

  wifiDevices.Add (wifiStaDevices);
  wifiDevices.Add (wifiApDevices);

  // Set channel width
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth", UintegerValue (chWidth));

  // Set guard interval
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/ShortGuardIntervalSupported", BooleanValue (shortGuardInterval));

  // Configure the mobility.
  /*
  * STA Mobility
  */

  MobilityHelper staMobility;

  double precision = 360*2;
  double radius = distance;
  double perimeter = 2*PI*radius;
  double tick = perimeter / ( precision * velocity);
  simulationTime = 5*(perimeter / velocity);
  std::cout << "Simulation Time: " << simulationTime << "\n";

  staMobility.SetMobilityModel("ns3::WaypointMobilityModel");
  staMobility.Install (wifiStaNodes.Get (0));
  Ptr<WaypointMobilityModel> ueWaypointMobility = DynamicCast<WaypointMobilityModel>( wifiStaNodes.Get(0)->GetObject<MobilityModel>());

  double angle = 0.0;
  Waypoint wpt (Seconds (0.0), Vector (radius, 0.0, 0.0));

  // Create waypoints
  for ( double curTime = 0; curTime < simulationTime; curTime += tick)
  {
    angle += (2*PI) / precision;
    wpt.time = Seconds (curTime);
    wpt.position.x = radius * cos(angle);
    wpt.position.y = radius * sin(angle);
    ueWaypointMobility->AddWaypoint(wpt);
  }

  /*
  * AP Mobility
  */
  MobilityHelper apMobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0));
  apMobility.SetPositionAllocator (positionAlloc);
  apMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  apMobility.Install (wifiApNodes.Get (0));

  //Configure the IP stack
  InternetStackHelper stack;
  stack.Install (wifiApNodes);
  stack.Install (wifiStaNodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");

  Ipv4InterfaceContainer staNodeInterface;
  Ipv4InterfaceContainer apNodeInterface;

  staNodeInterface = address.Assign (wifiStaDevices);
  apNodeInterface = address.Assign (wifiApDevices);

  uint16_t port = 9;


  //Configure the CBR generator
  PacketSinkHelper sink ("ns3::UdpSocketFactory", InetSocketAddress (apNodeInterface.GetAddress(0), port));
  ApplicationContainer apps_sink = sink.Install (wifiApNodes.Get (0));

  OnOffHelper onoff ("ns3::UdpSocketFactory", InetSocketAddress (apNodeInterface.GetAddress(0), port));
  onoff.SetConstantRate (DataRate (dataRate), 1420);
  onoff.SetAttribute ("StartTime", TimeValue (Seconds (1.000000)));
  ApplicationContainer clientApp = onoff.Install (wifiStaNodes.Get (0));

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  apps_sink.Start (Seconds (0.5));
  apps_sink.Stop (Seconds (simulationTime));

  //------------------------------------------------------------
  //-- Setup stats and data collection
  //--------------------------------------------

  if(verbose) {
    Config::ConnectWithoutContext ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/MonitorSnifferRx", MakeCallback(&DevMonitorSnifferRx));
    Config::ConnectWithoutContext ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/MonitorSnifferTx", MakeCallback (&DevMonitorSnifferTx));
  }
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();

  Packet::EnablePrinting ();
  Packet::EnableChecking ();


  Simulator::Schedule (Seconds (0), &MobilityPrint, wifiStaNodes.Get(0));

  Simulator::Stop (Seconds (simulationTime));
  Simulator::Run ();

  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats ();
  std::cout << "summary,Source Address,Destination Address,Tx Packets,Tx Bytes,Tx Offered,Rx Packets,Rx Bytes,Throughput\n";
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
  {
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
    std::cout << "summary," << t.sourceAddress << "," << t.destinationAddress << "," << i->second.txPackets << ","
    << i->second.txBytes  << "," << i->second.txBytes * 8.0 / (simulationTime-1.0) / 1000 / 1000 << ","
    << i->second.rxPackets << "," << i->second.rxBytes << ", " << i->second.rxBytes * 8.0 / (simulationTime-1.0) / 1000 / 1000  << "\n";
  }


  Simulator::Destroy ();

  return 0;
}
