/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/uinteger.h"
#include "ns3/boolean.h"
#include "ns3/double.h"
#include "ns3/string.h"
#include "ns3/log.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/wifi-phy.h"
#include "ns3/ssid.h"
#include "ns3/mobility-helper.h"
#include "ns3/mobility-model.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/trace-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/point-to-point-module.h"
#include "ns3/on-off-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/pointer.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/box.h"
#include "ns3/wifi-mac-header.h"
#include "ns3/waypoint.h"
#include "ns3/waypoint-mobility-model.h"
#include "ns3/wifi-net-device.h"
#include "ns3/global-value.h"
#include "ns3/integer.h"
#include <math.h>
#include <tuple>
#include <map>
#include <algorithm>
#include <string>
#include <iostream>

#define PI 3.14159265

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("vht-wifi-network");

/*
* Function used to dump the mobility of the node on stdout.
*/

void MobilityPrint (Ptr<Node> node) {
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  Vector pos = mobility->GetPosition ();
  Vector vel = mobility->GetVelocity ();

  Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
  Ipv4Address addr = ipv4->GetAddress (1, 0).GetLocal ();


  std::cout << "position," << Simulator::Now ().GetSeconds () << "," << addr << "," << pos << "," << vel << "\n";
  Simulator::Schedule (Seconds (0.1), &MobilityPrint, node);
}

/*
* Functions used to dump informations about frames on stdout
*/

void DevMonitorSnifferRx (Ptr< const Packet > packet, uint16_t channelFreqMhz, WifiTxVector txVector, struct MpduInfo aMpdu, struct SignalNoiseDbm signalNoise) {
  std::cout << "rx," << Simulator::Now ().GetSeconds () << "," << signalNoise.signal << ", " << txVector.GetMode() << ", " << txVector.GetGuardInterval() << ", " << txVector.GetChannelWidth() << ", " << txVector.GetNss() << "\n";
}


void DevMonitorSnifferTx (Ptr< const Packet > packet, uint16_t channelFreqMhz, WifiTxVector txVector, struct MpduInfo aMpdu) {
  std::cout << "tx," << Simulator::Now ().GetSeconds () << "," << txVector.GetMode() << ", " << txVector.GetGuardInterval() << ", " << +txVector.GetChannelWidth() << ", " << +txVector.GetNss() << ", " << txVector.GetMode().GetDataRate(txVector)/(1000*1000) << "\n";
}

void configureChannel(YansWifiPhyHelper *wifiPhy) {
  YansWifiChannelHelper helper;
  PointerValue ptr;
  Ptr<YansWifiChannel> channel;

  helper.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  // Fast Fading
  helper.AddPropagationLoss("ns3::NakagamiPropagationLossModel");
  // Slow Fading
  helper.AddPropagationLoss("ns3::LogDistancePropagationLossModel");

  channel = helper.Create();
  wifiPhy->SetChannel(channel);
  wifiPhy->SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
}


YansWifiPhyHelper configurePhy() {
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
  configureChannel(&wifiPhy);
  return wifiPhy;
}


WifiMacHelper configureMac(bool sta) {
  WifiMacHelper wifiMac;
  uint32_t BE_MaxAmpduSize = 65535;
  if(sta) {
    wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(Ssid("AP")), "BE_MaxAmpduSize", UintegerValue(BE_MaxAmpduSize));
  } else {
    wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(Ssid("AP")), "BE_MaxAmpduSize", UintegerValue(BE_MaxAmpduSize));
  }
  return wifiMac;
}


int main (int argc, char *argv[])
{

  uint32_t rtsThreshold = 65535;
  std::string manager = "ns3::IntelWifiManager";
  std::string dataRate = "500Mb/s";
  uint32_t BeMaxAmpduSize = 65535;
  bool shortGuardInterval = true;
  uint32_t chWidth = 20;

  double distance = 100;
  double position = 50;
  double velocity = 1;
  double simulationTime = 30;
  bool verbose = false;
  bool pcap = false;

  CommandLine cmd;
  cmd.AddValue ("simulationTime", "Simulation Time", simulationTime);
  cmd.AddValue ("pcap", "Pcap output", pcap);
  cmd.AddValue ("manager", "PRC Manager of the AP", manager);
  cmd.AddValue ("shortGuardInterval", "Enable Short Guard Interval in all stations", shortGuardInterval);
  cmd.AddValue ("channelWidth", "Channel width of all the stations", chWidth);
  cmd.AddValue ("rtsThreshold", "RTS threshold", rtsThreshold);
  cmd.AddValue ("BeMaxAmpduSize", "BE Mac A-MPDU size", BeMaxAmpduSize);
  cmd.AddValue ("distance", "Distance between the two extreme nodes", distance);
  cmd.AddValue ("position", "Position of the middle node", position);
  cmd.AddValue ("velocity", "Velocity of the AP", distance);
  cmd.AddValue ("dataRate", "Velocity of the STA", dataRate);
  cmd.AddValue ("verbose", "Verbose output (tx, rx)", verbose);
  cmd.Parse (argc,argv);

  std::string all_parameters = "_manager=" + manager +
  "_simulationTime=" + std::to_string(simulationTime) +
  "_pcap=" + std::to_string(pcap) +
  "_distance=" + std::to_string(distance) +
  "_velocity=" + std::to_string(velocity) +
  "_channelWidth=" + std::to_string(chWidth) +
  "_dataRate=" + dataRate;
  std::replace(all_parameters.begin(), all_parameters.end(), '/', '-');

  NodeContainer wifiStaNodes;
  wifiStaNodes.Create(2);

  NodeContainer wifiApNodes;
  wifiApNodes.Create(1);

  YansWifiPhyHelper phy = configurePhy();

  WifiHelper wifi;
  NetDeviceContainer wifiStaDevices, wifiApDevices, wifiDevices;

  wifi.SetStandard (WIFI_PHY_STANDARD_80211ac);

  WifiMacHelper staMac = configureMac(true);
  wifi.SetRemoteStationManager (manager);
  wifiStaDevices.Add(wifi.Install(phy, staMac, wifiStaNodes.Get(0)));
  wifiStaDevices.Add(wifi.Install(phy, staMac, wifiStaNodes.Get(1)));

  WifiMacHelper apMac = configureMac(false);
  wifi.SetRemoteStationManager (manager);
  wifiApDevices.Add(wifi.Install (phy, apMac, wifiApNodes.Get(0)));

  // Perform post-install configuration from defaults for channel width,
  // guard interval, and nss, if necessary
  // Obtain pointer to the WifiPhy
  Ptr<NetDevice> firstStaDevice = wifiStaDevices.Get (0);
  Ptr<NetDevice> secondStaDevice = wifiStaDevices.Get (1);
  Ptr<NetDevice> apDevice = wifiApDevices.Get(0);
  Ptr<WifiNetDevice> fwstaDevice = firstStaDevice->GetObject<WifiNetDevice> ();
  Ptr<WifiNetDevice> swstaDevice = secondStaDevice->GetObject<WifiNetDevice> ();
  Ptr<WifiNetDevice> wapDevice = apDevice->GetObject<WifiNetDevice> ();
  Ptr<WifiPhy> fwifiPhyPtrSta = fwstaDevice->GetPhy ();
  Ptr<WifiPhy> swifiPhyPtrSta = swstaDevice->GetPhy ();
  Ptr<WifiPhy> wifiPhyPtrAp = wapDevice->GetPhy ();

  uint8_t t_clientNss = 2;
  uint8_t t_serverNss = 2;

  fwifiPhyPtrSta->SetNumberOfAntennas (t_clientNss);
  fwifiPhyPtrSta->SetMaxSupportedTxSpatialStreams (t_clientNss);
  fwifiPhyPtrSta->SetMaxSupportedRxSpatialStreams (t_clientNss);
  fwifiPhyPtrSta->SetChannelWidth (chWidth);
  fwifiPhyPtrSta->SetShortGuardInterval (true);

  swifiPhyPtrSta->SetNumberOfAntennas (t_clientNss);
  swifiPhyPtrSta->SetMaxSupportedTxSpatialStreams (t_clientNss);
  swifiPhyPtrSta->SetMaxSupportedRxSpatialStreams (t_clientNss);
  swifiPhyPtrSta->SetChannelWidth (chWidth);
  swifiPhyPtrSta->SetShortGuardInterval (true);


  wifiPhyPtrAp->SetNumberOfAntennas (t_serverNss);
  wifiPhyPtrAp->SetMaxSupportedTxSpatialStreams (t_serverNss);
  wifiPhyPtrAp->SetMaxSupportedRxSpatialStreams (t_serverNss);
  wifiPhyPtrAp->SetChannelWidth (chWidth);
  wifiPhyPtrAp->SetShortGuardInterval (true);

  /*
  * STA Mobility
  */

  MobilityHelper staMobility;
  Ptr<ListPositionAllocator> staPositionAlloc = CreateObject<ListPositionAllocator> ();
  staPositionAlloc->Add (Vector (0.0, 0.0, 1.0));
  staPositionAlloc->Add (Vector (distance, 0.0, 1.0));

  staMobility.SetPositionAllocator (staPositionAlloc);
  staMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  staMobility.Install (wifiStaNodes.Get (0));
  staMobility.Install (wifiStaNodes.Get (1));

  /*
  * AP Mobility
  */
  /* MobilityHelper apMobility;

  double precision = 360*2;
  double radius = 15;
  double perimeter = 2 * PI * radius;
  double tick = perimeter / ( precision * velocity);

  apMobility.SetMobilityModel("ns3::WaypointMobilityModel");
  apMobility.Install (wifiApNodes.Get (0));

  Ptr<WaypointMobilityModel> ueWaypointMobility = DynamicCast<WaypointMobilityModel>( wifiApNodes.Get(0)->GetObject<MobilityModel>());

  double angle = 0.0;
  Waypoint wpt (Seconds (0.0), Vector ((distance/2)+radius, 0.0, 20.0));

  // Create waypoints
  for ( double curTime = 0; curTime < simulationTime; curTime += tick)
  {
  //  std::cout << "curTime: " << curTime << ", angle: " << angle << ", x: " << wpt.position.x << ", y: " << wpt.position.y << "\n";
  angle += (2*PI) / precision;
  wpt.time = Seconds (curTime);
  wpt.position.x = (distance/2) + radius * cos(angle);
  wpt.position.y = radius * sin(angle);
  ueWaypointMobility->AddWaypoint(wpt);
} */

MobilityHelper apMobility;
Ptr<ListPositionAllocator> apPositionAlloc = CreateObject<ListPositionAllocator> ();
apPositionAlloc->Add (Vector (position, 0.0, 1.0));

apMobility.SetPositionAllocator (apPositionAlloc);
apMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
apMobility.Install (wifiApNodes.Get (0));


/*
* Internet stack
*/
//Configure the IP stack
InternetStackHelper stack;
stack.Install (wifiApNodes);
stack.Install (wifiStaNodes);

Ipv4AddressHelper address;
address.SetBase ("10.1.1.0", "255.255.255.0");

Ipv4InterfaceContainer staNodeInterface;
Ipv4InterfaceContainer apNodeInterface;

staNodeInterface = address.Assign (wifiStaDevices);
apNodeInterface = address.Assign (wifiApDevices);

uint16_t port = 9;


//Configure the CBR generator
PacketSinkHelper sink ("ns3::UdpSocketFactory", InetSocketAddress (staNodeInterface.GetAddress(0), port));
ApplicationContainer apps_sink = sink.Install (wifiStaNodes.Get (0));

OnOffHelper onoff ("ns3::UdpSocketFactory", InetSocketAddress (staNodeInterface.GetAddress(0), port));
onoff.SetConstantRate (DataRate (dataRate), 1420);
onoff.SetAttribute ("StartTime", TimeValue (Seconds (1.000000)));
ApplicationContainer clientApp = onoff.Install (wifiStaNodes.Get (1));

Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
apps_sink.Start (Seconds (0.5));
apps_sink.Stop (Seconds (simulationTime));


/*
* PCAP Output
*/
StringValue rngrun;
GlobalValue::GetValueByName("RngRun", rngrun);
std::cout << "rngrun: " << rngrun.Get() << "\n";
if(pcap && (rngrun.Get() == "0"))
{
  phy.EnablePcap("AP"+all_parameters, wifiApNodes);
  phy.EnablePcap("STA"+all_parameters, wifiStaNodes);
}

//
// Calculate Throughput using Flowmonitor
//
FlowMonitorHelper flowmon;
Ptr<FlowMonitor> monitor = flowmon.InstallAll();

Packet::EnablePrinting ();
Packet::EnableChecking ();

Simulator::Schedule (Seconds (0), &MobilityPrint, wifiStaNodes.Get(0));
Simulator::Schedule (Seconds (0), &MobilityPrint, wifiStaNodes.Get(1));
Simulator::Schedule (Seconds (0), &MobilityPrint, wifiApNodes.Get(0));

if(verbose) {
  Config::ConnectWithoutContext ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/MonitorSnifferRx", MakeCallback(&DevMonitorSnifferRx));
  Config::ConnectWithoutContext ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/MonitorSnifferTx", MakeCallback (&DevMonitorSnifferTx));

}
Simulator::Stop (Seconds (simulationTime));
Simulator::Run ();


// 10. Print per flow statistics
monitor->CheckForLostPackets ();
Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats ();
std::cout << "summary,Source Address,Destination Address,Tx Packets,Tx Bytes,Tx Offered,Rx Packets,Rx Bytes,Throughput\n";
for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
{
  Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
  std::cout << "summary," << t.sourceAddress << "," << t.destinationAddress << "," << i->second.txPackets << ","
  << i->second.txBytes  << "," << i->second.txBytes * 8.0 / (simulationTime-1.0) / 1000 / 1000 << ","
  << i->second.rxPackets << "," << i->second.rxBytes << ", " << i->second.rxBytes * 8.0 / (simulationTime-1.0) / 1000 / 1000  << "\n";
}

Simulator::Destroy ();
return 0;
}
