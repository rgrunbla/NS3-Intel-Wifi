/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/uinteger.h"
#include "ns3/boolean.h"
#include "ns3/double.h"
#include "ns3/string.h"
#include "ns3/log.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/wifi-phy.h"
#include "ns3/ssid.h"
#include "ns3/mobility-helper.h"
#include "ns3/mobility-model.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/trace-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/point-to-point-module.h"
#include "ns3/on-off-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/pointer.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/box.h"
#include "ns3/wifi-mac-header.h"
#include "ns3/waypoint.h"
#include "ns3/waypoint-mobility-model.h"
#include "ns3/wifi-net-device.h"
#include <math.h>
#include <tuple>
#include <map>
#include <iostream>

#define PI 3.14159265

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("vht-wifi-network");

/*
* Function used to dump the mobility of the node on stdout.
*/

void MobilityPrint (Ptr<Node> node) {
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  Vector pos = mobility->GetPosition ();
  Vector vel = mobility->GetVelocity ();

  Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
  Ipv4Address addr = ipv4->GetAddress (1, 0).GetLocal ();


  std::cout << "position," << Simulator::Now ().GetSeconds () << "," << addr << "," << pos << "," << vel << "\n";
  Simulator::Schedule (Seconds (0.1), &MobilityPrint, node);
}

/*
* Functions used to dump informations about frames on stdout
*/

void DevMonitorSnifferRx (Ptr< const Packet > packet, uint16_t channelFreqMhz, WifiTxVector txVector, struct MpduInfo aMpdu, struct SignalNoiseDbm signalNoise) {
  std::cout << "rx," << Simulator::Now ().GetSeconds () << "," << signalNoise.signal << ", " << txVector.GetMode() << ", " << txVector.GetGuardInterval() << ", " << txVector.GetChannelWidth() << ", " << txVector.GetNss() << "\n";
}


void DevMonitorSnifferTx (Ptr< const Packet > packet, uint16_t channelFreqMhz, WifiTxVector txVector, struct MpduInfo aMpdu) {
  std::cout << "tx," << Simulator::Now ().GetSeconds () << "," << txVector.GetMode() << ", " << txVector.GetGuardInterval() << ", " << +txVector.GetChannelWidth() << ", " << +txVector.GetNss() << ", " << txVector.GetMode().GetDataRate(txVector)/(1000*1000) << "\n";
}

void configureChannel(YansWifiPhyHelper *wifiPhy) {
  YansWifiChannelHelper helper;
  PointerValue ptr;
  Ptr<YansWifiChannel> channel;
  Ptr<LogDistancePropagationLossModel> PropagationLossModel;

  helper.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  helper.AddPropagationLoss("ns3::LogDistancePropagationLossModel");
  helper.AddPropagationLoss("ns3::NakagamiPropagationLossModel");
  helper.AddPropagationLoss("ns3::RandomWallPropagationLossModel",
  "Walls", DoubleValue(4),
  "Radius", DoubleValue(15),
  "wallLoss", DoubleValue(5));

  channel = helper.Create();
  channel->GetAttribute("PropagationLossModel", ptr);

  wifiPhy->SetChannel(channel);
}


YansWifiPhyHelper configurePhy() {
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
  configureChannel(&wifiPhy);
  return wifiPhy;
}


WifiMacHelper configureMac(bool sta) {
  WifiMacHelper wifiMac;
  uint32_t BE_MaxAmpduSize = 65535;
  if(sta) {
    wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(Ssid("AP")), "BE_MaxAmpduSize", UintegerValue(BE_MaxAmpduSize));
  } else {
    wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(Ssid("AP")), "BE_MaxAmpduSize", UintegerValue(BE_MaxAmpduSize));
  }
  return wifiMac;
}


int main (int argc, char *argv[])
{

  uint32_t rtsThreshold = 65535;
  std::string manager = "ns3::IntelWifiManager";
  std::string dataRate = "200Mb/s";
  uint32_t chWidth = 40;
  double distance = 30;
  double velocity = 1;
  double simulationTime = 0;
  bool verbose = false;
  bool pcap = false;

  CommandLine cmd;
  cmd.AddValue ("pcap", "Pcap output", pcap);
  cmd.AddValue ("manager", "Manager of the AP and the STA", manager);
  cmd.AddValue ("channelWidth", "Channel width of all the stations", chWidth);
  cmd.AddValue ("rtsThreshold", "RTS threshold", rtsThreshold);
  cmd.AddValue ("distance", "Distance between the STA and the AP", distance);
  cmd.AddValue ("velocity", "Velocity of the STA", velocity);
  cmd.AddValue ("dataRate", "Velocity of the STA", dataRate);
  cmd.AddValue ("verbose", "Verbose output (tx, rx)", verbose);
  cmd.Parse (argc,argv);

  NodeContainer wifiStaNodes;
  wifiStaNodes.Create(1);

  NodeContainer wifiApNodes;
  wifiApNodes.Create(1);

  YansWifiPhyHelper phy = configurePhy();

  WifiHelper wifi;
  NetDeviceContainer wifiStaDevices, wifiApDevices, wifiDevices;

  wifi.SetStandard (WIFI_PHY_STANDARD_80211ac);

  WifiMacHelper staMac = configureMac(true);
  wifi.SetRemoteStationManager (manager);
  wifiStaDevices.Add(wifi.Install(phy, staMac, wifiStaNodes.Get(0)));

  WifiMacHelper apMac = configureMac(false);
  wifi.SetRemoteStationManager (manager);
  wifiApDevices.Add(wifi.Install (phy, apMac, wifiApNodes.Get(0)));

// Perform post-install configuration from defaults for channel width,
// guard interval, and nss, if necessary
// Obtain pointer to the WifiPhy
  Ptr<NetDevice> staDevice = wifiStaDevices.Get (0);
  Ptr<NetDevice> apDevice = wifiApDevices.Get(0);
  Ptr<WifiNetDevice> wstaDevice = staDevice->GetObject<WifiNetDevice> ();
  Ptr<WifiNetDevice> wapDevice = apDevice->GetObject<WifiNetDevice> ();
  Ptr<WifiPhy> wifiPhyPtrSta = wstaDevice->GetPhy ();
  Ptr<WifiPhy> wifiPhyPtrAp = wapDevice->GetPhy ();

  uint8_t t_clientNss = 2;
  uint8_t t_serverNss = 2;


  std::cout << +t_clientNss << "\n";
  std::cout << +t_serverNss << "\n";

  wifiPhyPtrSta->SetNumberOfAntennas (t_clientNss);
  wifiPhyPtrSta->SetMaxSupportedTxSpatialStreams (t_clientNss);
  wifiPhyPtrSta->SetMaxSupportedRxSpatialStreams (t_clientNss);
  wifiPhyPtrSta->SetChannelWidth (chWidth);

  wifiPhyPtrAp->SetNumberOfAntennas (t_serverNss);
  wifiPhyPtrAp->SetMaxSupportedTxSpatialStreams (t_serverNss);
  wifiPhyPtrAp->SetMaxSupportedRxSpatialStreams (t_serverNss);
  wifiPhyPtrAp->SetChannelWidth (chWidth);

  wifiPhyPtrSta->SetShortGuardInterval (true);
  wifiPhyPtrAp->SetShortGuardInterval (true);

  /*
  * STA Mobility
  */

  MobilityHelper staMobility;

  double precision = 360*2;
  double radius = distance;
  double perimeter = 2*PI*radius;
  double tick = perimeter / ( precision * velocity);
  simulationTime = 5*(perimeter / velocity);

  staMobility.SetMobilityModel("ns3::WaypointMobilityModel");
  staMobility.Install (wifiStaNodes.Get (0));
  Ptr<WaypointMobilityModel> ueWaypointMobility = DynamicCast<WaypointMobilityModel>( wifiStaNodes.Get(0)->GetObject<MobilityModel>());

  double angle = 0.0;
  Waypoint wpt (Seconds (0.0), Vector (radius, 0.0, 0.0));

  // Create waypoints
  for ( double curTime = 0; curTime < simulationTime; curTime += tick)
  {
  //  std::cout << "curTime: " << curTime << ", angle: " << angle << ", x: " << wpt.position.x << ", y: " << wpt.position.y << "\n";
    angle += (2*PI) / precision;
    wpt.time = Seconds (curTime);
    wpt.position.x = radius * cos(angle);
    wpt.position.y = radius * sin(angle);
    ueWaypointMobility->AddWaypoint(wpt);
  }

  /*
  * AP Mobility
  */
  MobilityHelper apMobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0));

  apMobility.SetPositionAllocator (positionAlloc);
  apMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");

  apMobility.Install (wifiApNodes);

  /*
  * Internet stack
  */
  //Configure the IP stack
  InternetStackHelper stack;
  stack.Install (wifiApNodes);
  stack.Install (wifiStaNodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");

  Ipv4InterfaceContainer staNodeInterface;
  Ipv4InterfaceContainer apNodeInterface;

  staNodeInterface = address.Assign (wifiStaDevices);
  apNodeInterface = address.Assign (wifiApDevices);

  uint16_t port = 9;


  //Configure the CBR generator
  PacketSinkHelper sink ("ns3::UdpSocketFactory", InetSocketAddress (apNodeInterface.GetAddress(0), port));
  ApplicationContainer apps_sink = sink.Install (wifiApNodes.Get (0));

  OnOffHelper onoff ("ns3::UdpSocketFactory", InetSocketAddress (apNodeInterface.GetAddress(0), port));
  onoff.SetConstantRate (DataRate (dataRate), 1420);
  onoff.SetAttribute ("StartTime", TimeValue (Seconds (1.000000)));
  ApplicationContainer clientApp = onoff.Install (wifiStaNodes.Get (0));

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  apps_sink.Start (Seconds (0.5));
  apps_sink.Stop (Seconds (simulationTime));


  /*
   * PCAP Output
   */

   if(pcap) {
     phy.EnablePcap("AP", wifiApNodes);
     phy.EnablePcap("STA", wifiApNodes);
   }

  //
  // Calculate Throughput using Flowmonitor
  //
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();

  Packet::EnablePrinting ();
  Packet::EnableChecking ();

  Simulator::Schedule (Seconds (0), &MobilityPrint, wifiStaNodes.Get(0));

  if(verbose) {
    Config::ConnectWithoutContext ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/MonitorSnifferRx", MakeCallback(&DevMonitorSnifferRx));
    Config::ConnectWithoutContext ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/MonitorSnifferTx", MakeCallback (&DevMonitorSnifferTx));

  }
  Simulator::Stop (Seconds (simulationTime));
  Simulator::Run ();


  // 10. Print per flow statistics
  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats ();
  std::cout << "summary,Source Address,Destination Address,Tx Packets,Tx Bytes,Tx Offered,Rx Packets,Rx Bytes,Throughput\n";
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
  {
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
    std::cout << "summary," << t.sourceAddress << "," << t.destinationAddress << "," << i->second.txPackets << ","
    << i->second.txBytes  << "," << i->second.txBytes * 8.0 / (simulationTime-1.0) / 1000 / 1000 << ","
    << i->second.rxPackets << "," << i->second.rxBytes << ", " << i->second.rxBytes * 8.0 / (simulationTime-1.0) / 1000 / 1000  << "\n";
  }

  Simulator::Destroy ();
  return 0;
}
