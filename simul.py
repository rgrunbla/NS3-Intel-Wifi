#!/usr/bin/env python3
import csv
import itertools
import random
import subprocess
import os
import sys
from multiprocessing.pool import ThreadPool
import shutil
from pathlib import Path
import glob
import argparse
import yaml
from collections import defaultdict 

my_env = os.environ.copy()

parser = argparse.ArgumentParser()
parser.add_argument("config", type=str, help="The name of the yaml config file used for simulation")
args = parser.parse_args()

with open(args.config, 'r') as stream:
    try:
        config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

if config['version'] != "dev":
    ns3_directory = Path("./ns-allinone-%s/ns-%s" % (config['version'], config['version']))
else:
    ns3_directory = Path("./ns-3-dev-master/")

PWD = os.path.dirname(os.path.abspath(__file__))

random.seed("RG,IGL,OS")

ignored_keys = set({"version", "script", "repeats"})
options_names = []
options_values = []

for key, value in config.items():
    if key in ignored_keys:
        continue
    print("--%s: %s" % (key, value))
    options_names.append(key)
    options_values.append(value)

repeats = config["repeats"]
script_name = config["script"]

combinations = list(itertools.product(*options_values))
print("%s combinations. Will take some time…" % (repeats*len(combinations)))


def create_filename(tag, filename, run):
    return "OUTPUT/%s_%s_run=%s.csv" % (tag, filename, run)


def work(combination):
    arguments = []
    filename_pieces = [script_name, str(repeats)]
    for i, value in enumerate(combination):
        arguments.append("--%s=%s" % (options_names[i], value))
        filename_pieces.append("%s=%s" % (options_names[i], value))

    filename = "_".join(filename_pieces).replace("/", "-")
    cli_string = " ".join(arguments)

    for run in range(repeats):
        finished_filename = create_filename("finished", filename, run)
        exists = os.path.isfile(finished_filename)

        if exists:
            print("Skipping %s" % finished_filename)
            return

        command_line = ['./build/scratch/%s %s --RngRun=%s' % (script_name, cli_string, run)]
        print(" ".join(command_line))
        my_subprocess = subprocess.check_output(command_line, cwd=str(ns3_directory), env=my_env, shell = True, stderr=subprocess.STDOUT)
        output = my_subprocess.decode("utf-8")
       

        logs = defaultdict(list)

        for line in output.split("\n")[:-1]:
            splitted = line.split(",")
            if(len(splitted) > 0):
                tag = splitted[0]
                logs[tag].append(splitted[1:])

        for tag in set(logs.keys()):
            print(create_filename(tag, filename, run))
            with open(create_filename(tag, filename, run), "w") as csvfile:
                writer = csv.writer(csvfile, delimiter=",")
                for element in logs[tag]:
                    writer.writerow(element)
                csvfile.flush()
                csvfile.close()

        open(finished_filename, 'a').close()

num = None
tp = ThreadPool(num)

random.shuffle(combinations)

for combination in combinations:
    tp.apply_async(work, (combination,))

tp.close()
tp.join()
