SHELL := /bin/bash
VERSION := 3.30
SCRIPT := distance

.PHONY: help download extract prepare build test simul

.DEFAULT: help

all: download extract prepare build test

help:
	@echo "make download"
	@echo "       Download the NS-3 tarball"
	@echo "make extract"
	@echo "       Extract the NS-3 tarball"
	@echo "make prepare"
	@echo "       Copy custom models to NS-3 directory"
	@echo "make build"
	@echo "       Build NS-3"
	@echo "make test"
	@echo "       Check that NS-3 is working"
	@echo "make simul"
	@echo "       Launch the NS-3 simulation"

download:
	@echo "Downloading NS-3 version $(VERSION)"
	wget --continue "https://www.nsnam.org/releases/ns-allinone-$(VERSION).tar.bz2" 

download-dev:
	@echo "Downloading NS-3 dev tree"
	wget --continue "https://gitlab.com/nsnam/ns-3-dev/-/archive/master/ns-3-dev-master.tar.gz"

extract: download
	@echo "Extracting NS-3 version $(VERSION)"
	tar -xf ns-allinone-$(VERSION).tar.bz2

extract-dev: download-dev
	@echo "Extracting NS-3 dev tree"
	tar -xf ns-3-dev-master.tar.gz

prepare: extract
	cp ./PropagationModel/propagation-loss-model.{h,cc} ns-allinone-$(VERSION)/ns-$(VERSION)/src/propagation/model/
	cp ./Iwl-Mvm-Rs/intel-rate-wifi-manager.{h,cc} ns-allinone-$(VERSION)/ns-$(VERSION)/src/wifi/model/
	cp ./wscript_$(VERSION) ns-allinone-$(VERSION)/ns-$(VERSION)/src/wifi/wscript
	cp ./clang_compilation_database.py ./ns-allinone-$(VERSION)/ns-$(VERSION)/waf-tools/clang_compilation_database.py
	cp -r ./scratch ./ns-allinone-$(VERSION)/ns-$(VERSION)/
	cp ./qos-utils.cc ./ns-allinone-$(VERSION)/ns-$(VERSION)/src/wifi/model/qos-utils.cc

prepare-dev: extract-dev
	cp ./PropagationModel/propagation-loss-model.{h,cc} ns-3-dev-master/src/propagation/model/
	cp ./Iwl-Mvm-Rs/intel-rate-wifi-manager-dev.h ns-3-dev-master/src/wifi/model/intel-rate-wifi-manager.h
	cp ./Iwl-Mvm-Rs/intel-rate-wifi-manager-dev.cc ns-3-dev-master/src/wifi/model/intel-rate-wifi-manager.cc
	cp ./wscript_dev ns-3-dev-master/src/wifi/wscript
	cp ./clang_compilation_database.py ./ns-3-dev-master/waf-tools/clang_compilation_database.py
	cp -r ./scratch ./ns-3-dev-master/


build: prepare
	cd ns-allinone-$(VERSION)/ns-$(VERSION) && ./waf configure
	cd ns-allinone-$(VERSION)/ns-$(VERSION) && ./waf build

build-dev: prepare-dev
	cd ns-3-dev-master/ && ./waf configure
	cd ns-3-dev-master/ && ./waf build

test: build
	cd ns-allinone-$(VERSION)/ns-$(VERSION) && echo "./build/scratch/example" | ./waf shell

test-dev: build-dev
	cd ns-3-dev-master && echo "./build/scratch/example" | ./waf shell

simul: build
	cd ns-allinone-$(VERSION)/ns-$(VERSION) && echo "cd ../../ && ./simul.py $(SCRIPT).yaml" | ./waf shell

simul-dev: build-dev
	cd ns-3-dev-master && echo "cd ../ && ./simul.py $(SCRIPT).yaml" | ./waf shell
